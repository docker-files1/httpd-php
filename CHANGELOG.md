# [1.8.0](/Docker_Files/httpd-php/compare/1.8.0...master)
* CICD migraton from Jenkins to GitlabCI

# [1.7.0](/Docker_Files/httpd-php/compare/1.7.0...master)
* Docker linter apply and little hotfix that might affect to some files that match with the current `.gitignore`

# [1.6.0](/Docker_Files/httpd-php/compare/1.6.0...master)
* Se actualiza el formato de la documentación
* Se elimina código comentado en el *Dockerfile*
* Se ajusta el *timewait* subiendolo a `30`

# [1.5.0](/Docker_Files/httpd-php/compare/1.5.0...master)
* Se actualiza el jenkinsfile segun la nueva versión de la *Shared Library*
* Se retira soporte para armv7

# [1.4.0](/Docker_Files/httpd-php/compare/1.4.0...master)
* Se actualiza php a la versión **7.4**
* Se expone la opción de `disable_functions` y `memory_limit` de `php`.
* Se le da soporte a la arch `aarch64` para la extensión *ioncube_loader*

# [1.3.1](/Docker_Files/httpd-php/compare/1.3.1...master)
* Se activa la extensión `exif`

# [1.3.0](/Docker_Files/httpd-php/compare/1.3.0...master)
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# [1.2.1](/Docker_Files/httpd-php/compare/1.2.1...master)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# [1.2.0](/Docker_Files/httpd-php/compare/1.2.0...master)
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [1.1.1](/Docker_Files/httpd-php/compare/1.1.1...master)
* Se ajusta el `Jenkinsfile` para mejorar el formato del *disclaimer* en cuanto a las `Jenkins shared libs`

# [1.1.0](/Docker_Files/httpd-php/compare/1.1.0...master)
* Se ajusta el `README` para que muestre información relevante
* Se ajusta el `Jenkinsfile` para informar sobre el path donde se encuentra las `Shared libs`

# [1.0.0](/Docker_Files/httpd-php/compare/1.0.0...master)
* Se optimiza el `Dockerfile` para reducir el número e capas de la imagen
* Se actualiza la configuración por defecto del *.editorconfig* y el *.gitignore*

# [0.9.1](/Docker_Files/httpd-php/compare/0.9.1...master)
* No se habían eliminado todas las etiquetas por defecto del *Dockerfile*

# [0.9.0](/Docker_Files/httpd-php/compare/0.9.0...master)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `dockerfilePipeline`
    * Se eliminan del Dockerfile, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.

# [0.8.0](/Docker_Files/httpd-php/compare/0.8.0...master)
* Se actualiza la receta para dar soporte a la arquitectura `linux/arm64/v8`
* Se cambia el uso de `booleano`, en ves de **0 / 1** se usa **true / false**

# [0.7.0](/Docker_Files/httpd-php/compare/0.7.0...master)
* Inicialización del `CHANGELOG`
* Lintado del ficheros `Vagrantfile`
* Docker compose para pruebas ágiles
* Se actualiza versión a 3 dígitos
* Se agrega fichero de licencia de código
* Se optimiza la descarga del paquete `ioncube` en función de la arquitectura
