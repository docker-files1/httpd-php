[![pipeline status](https://gitlab.com/docker-files1/httpd-php/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/httpd-php/-/commits/development) ![Project version](https://img.shields.io/docker/v/oscarenzo/httpd-php?sort=date) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/httpd-php) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/httpd-php?sort=date) ![Project license](https://img.shields.io/gitlab/license/docker-files1/httpd-php)

# httpd-php
Dockerfile to create containers with APACHE+PHP.

## 🧾 Components
 - Debian
 - apache 2.4
 - php 7.4

### 📦 APACHE
#### Compliance
 - Disable the ServerSignature Directive
 - Set the ServerTokens Directive to Prod
 - Set the TraceEnable to Off
 - Disable Directory Listing

#### Customization
 - Enable Prefork
 - Enable .htaccess for /var/www/html/

#### Static Modules
- core_module
- so_module
- watchdog_module
- http_module
- log_config_module
- logio_module
- version_module
- unixd_module

#### Shared Modules
- access_compat_module
- alias_module
- auth_basic_module
- authn_core_module
- authn_file_module
- authz_core_module
- authz_host_module
- authz_user_module
- autoindex_module
- deflate_module
- dir_module
- env_module
- filter_module
- headers_module
- mime_module
- mpm_prefork_module
- negotiation_module
- php7_module
- reqtimeout_module
- setenvif_module
- status_module
- rewrite_module

### 📦 PHP
#### Compliance
Disabled some functions like `show_source,system,shell_exec,passthru,exec,popen,phpinfo,proc_open,allow_url_fopen`

#### Customization
Set timezone to `Europe/Madrid`

#### Modules
- mbstring
- bcmath
- gd
- imap
- intl
- mcrypt
- process
- xml
- xmlrpc
- soap
- zip
- ioncube-loader
- pdo_mysql
- mysqli
- mysqlnd
- exif

## 🔗 References
https://github.com/vfalies/EnvDevPHPBase/blob/php7.2-fpm/Dockerfile

## ✍️ Advices
For persistent volume configuration you may think on this paths:

```
/var/www/html/
```

## ⚙️ Environment variables
This image uses environment variables to allow the configuration of some parameters at run time:

* Variable name: `NOSNIFF`
* Default value: true
* Accepted values: A boolean.
* Description: Prevent MSIE from interpreting files as something else than declared by the content type in the HTTP headers. If you don't specify it through the `NOSNIFF` environment variable at run time, `true` will be used by default.

----

* Variable name: `SAMEORIGIN`
* Default value: true
* Accepted values: A boolean.
* Description: Prevent other sites from embedding pages from this site as frames. This defends against clickjacking attacks. If you don't specify it through the `SAMEORIGIN` environment variable at run time, `true` will be used by default.

----

* Variable name: `TZ`
* Default value: Europe/Madrid
* Accepted values: A TimeZone string format.
* Description: The TimeZone that apply to the OS and PHP. If you don't specify it through the `TZ` environment variable at run time, `Europe/Madrid` will be used by default.

----

* Variable name: `DISABLE_FUNCTIONS`
* Default value: show_source,system,shell_exec,passthru,exec,popen,phpinfo,proc_open,allow_url_fopen
* Accepted values: All php function supported comma separated.
* Description: The function that you want to disable from PHP. If you don't specify it through the `DISABLE_FUNCTIONS` environment variable at run time, `show_source,system,shell_exec,passthru,exec,popen,phpinfo,proc_open,allow_url_fopen` will be used by default.

----

* Variable name: `MEMORY_LIMIT`
* Default value: 128M
* Accepted values: Value in Megabytes for assign PHP memory.
* Description: The PHP memory limit for PHP. If you don't specify it through the `MEMORY_LIMIT` environment variable at run time, `128M` will be used by default.

## 💬 Legend
* NKS => No key sensitive
* KS => Key sensitive
