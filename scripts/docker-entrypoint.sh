#!/bin/bash
set -e

if [ "$1" = "apache2-foreground" ]; then
	APA_MAINDIR="/etc/apache2"
	PHP_CONFDIR="/usr/local/etc/php/conf.d"
	PHP_EXTDIR="/usr/local/lib/php/extensions"
	HARDWARE="$(uname -m)"

	if [ -z "$NOSNIFF" ]; then
		export NOSNIFF="true"
	fi

	if [ -z "$SAMEORIGIN" ]; then
		export SAMEORIGIN="true"
	fi

	if [ -z "$TZ" ]; then
		export TZ="Europe/Madrid"
	fi

	if [ -z "$DISABLE_FUNCTIONS" ]; then
		export DISABLE_FUNCTIONS="show_source,system,shell_exec,passthru,exec,popen,phpinfo,proc_open,allow_url_fopen"
	fi

	if [ -z "$MEMORY_LIMIT" ]; then
		export MEMORY_LIMIT="128M"
	fi

	# Enable iocube loader
	if [ ! -f "${PHP_CONFDIR}/docker-php-ext-ioncube_loader.ini" ]; then
		# Detect hardware archiecture for ioncube installation
		# wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
		# wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_armv7l.tar.gz

		cd /tmp/ || exit 1
		if [ "$HARDWARE" == "armv7l" ]; then
			wget https://docker.arroyof.com/downloads/ioncube_loaders_lin_armv7l.tar.gz
			tar xzvf ioncube_loaders_lin_armv7l.tar.gz
		elif [ "$HARDWARE" == "aarch64"  ]; then
			wget https://docker.arroyof.com/downloads/ioncube_loaders_lin_aarch64.tar.gz
			tar xzvf ioncube_loaders_lin_aarch64.tar.gz
		else
			wget https://docker.arroyof.com/downloads/ioncube_loaders_lin_x86-64.tar.gz
			tar xzvf ioncube_loaders_lin_x86-64.tar.gz
		fi

		mv ioncube/ioncube_loader_lin_7.4.so "${PHP_EXTDIR}/no-debug-non-zts-20190902/"
		echo "zend_extension=$PHP_EXTDIR/no-debug-non-zts-20190902/ioncube_loader_lin_7.4.so" > "${PHP_CONFDIR}/docker-php-ext-ioncube_loader.ini"
		rm -rf ioncube*
	fi

	# Custom php configurations
	{
		# Securing php
		echo "disable_functions = $DISABLE_FUNCTIONS"
		# Customize php timezone
		echo "date.timezone = $TZ"
		# Customize php memory limit
		echo "memory_limit = $MEMORY_LIMIT"
	} > "${PHP_CONFDIR}/docker-php-custom-config.ini"

	# Custom apache configurations [Securing]
	grep -q "^ServerSignature" "$APA_MAINDIR"/conf-available/security.conf && \
	sed -i 's/^ServerSignature.*/ServerSignature Off/g' "$APA_MAINDIR"/conf-available/security.conf \
	|| echo "ServerSignature Off" >> "$APA_MAINDIR"/conf-available/security.conf

	grep -q "^ServerTokens" "$APA_MAINDIR"/conf-available/security.conf && \
	sed -i 's/^ServerTokens.*/ServerTokens Prod/g' "$APA_MAINDIR"/conf-available/security.conf \
	|| echo "ServerTokens Prod" >> "$APA_MAINDIR"/conf-available/security.conf

	grep -q "^TraceEnable" "$APA_MAINDIR"/conf-available/security.conf && \
	sed -i 's/^TraceEnable.*/TraceEnable Off/g' "$APA_MAINDIR"/conf-available/security.conf \
	|| echo "TraceEnable Off" >> "$APA_MAINDIR"/conf-available/security.conf

	# Enable apache rewrite module
	a2query -m rewrite 2> /dev/null || a2enmod rewrite

	# Enable apache headers module
	if [ "$NOSNIFF" == "true" ] || [ "$SAMEORIGIN" == "true" ]; then
		a2query -m headers 2> /dev/null || a2enmod headers
	fi

	# Apply header protection over XSS atacks
	if [ "$NOSNIFF" == "true" ]; then
		grep -q "nosniff" "$APA_MAINDIR"/conf-available/security.conf && \
		sed -i 's/.*nosniff.*/Header set X-Content-Type-Options\: \"nosniff\"/g' "$APA_MAINDIR"/conf-available/security.conf \
		|| echo "Header set X-Content-Type-Options: \"nosniff\"" >> "$APA_MAINDIR"/conf-available/security.conf
	fi

	if [ "$SAMEORIGIN" == "true" ]; then
		grep -q "sameorigin" "$APA_MAINDIR"/conf-available/security.conf && \
		sed -i 's/.*sameorigin.*/Header set X-Frame-Options\: \"sameorigin\"/g' "$APA_MAINDIR"/conf-available/security.conf \
		|| echo "Header set X-Content-Type-Options: \"sameorigin\"" >> "$APA_MAINDIR"/conf-available/security.conf
	fi

	# Set timezone
	ln -sf /usr/share/zoneinfo/"$TZ" /etc/localtime &> /dev/null && echo "Timezone configuration [ OK ]" || exit 2

cat << EOB

****************************************************
*                                                  *
*    Docker image: oscarenzo/httpd-php             *
*    https://gitlab.com/docker-files1/httpd-php    *
*                                                  *
****************************************************

SERVER SETTINGS
---------------
· Secured php: Ok [compiled with ioncube loader]
· Php/OS TimeZone: $TZ
· Secured Apache: Ok [Able to use .htaccess]
· Apache MPM: Prefork
· Nosniff Header: $NOSNIFF
· Sameorigin Header: $SAMEORIGIN
---------------

EOB
	set -- "$@"
fi

exec "$@"
