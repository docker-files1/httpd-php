FROM php:7.4-apache

LABEL ApacheBase="2.4"
LABEL DistBase="Debian 10 - Buster"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update -y && \
	apt-get install -y libzip-dev libpng-dev libc-client-dev libkrb5-dev netcat \
	libxml2-dev libicu-dev libmcrypt-dev wget --no-install-recommends && \
	apt-get remove -y curl && apt-get autoremove -y && apt-get clean -y && \
	rm -rf /var/lib/apt/lists/* && \
	docker-php-ext-configure imap --with-kerberos --with-imap-ssl && \
	printf "\n" | pecl install mcrypt && \
	docker-php-ext-enable mcrypt && \
	docker-php-ext-install bcmath gd exif imap \
	xmlrpc soap zip intl pdo_mysql mysqli

# Apache-php pre-configurations
COPY scripts/docker-entrypoint.sh /var/tmp/

RUN chmod +x /var/tmp/docker-entrypoint.sh

ENTRYPOINT ["/var/tmp/docker-entrypoint.sh"]

EXPOSE 80/tcp

HEALTHCHECK --interval=5m --timeout=3s \
  CMD nc -z localhost 80 || exit 1

CMD ["apache2-foreground"]
# docker build -t oscarenzo/httpd-php:latest .
# docker buildx build --push -t oscarenzo/httpd-php:latest --platform linux/amd64,linux/arm/v7,linux/arm64/v8 .
